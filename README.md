# Hackathon 2019 Backend

This is the backend for the Hackathon 2019. The topic for the hackathon was `smart substainable living`. 
Our problem that we tried to solve was around the topic transport with a focus on fuel.
The result is a prototype for slip stream driving.

## Technical stuff
This backend is totally written in Kotlin with the framework Ktor. 
It exposes a single API for the client that send the sensor data and for a pulling client.

## Setup

### Requirements
To run this project you need jdk-11 installed on your machine. 
 
### Building from Source
To build the project you need to execute the building process from maven.

Linux & MacOs: `./mvnw clean install`

Windows: `mavne.cmd clean install`

The newly built JAR is now located in the `./target` directory.

To start the application run: `java -jar hackathon-2019-backend-0.0.1-jar-with-dependencies.jar`

##API


