export function animateValue(element, start, end, duration) {
    if (start !== end) {
        let range = end - start;
        let current = start;
        let increment = end > start ? 1 : -1;
        let stepTime = Math.abs(Math.floor(duration / range));
        let timer = setInterval(function () {
            current += increment;
            element.innerText = current;
            if (current === end) {
                clearInterval(timer);
            }
        }, stepTime);
    }
}