import {animateValue} from './animation.js';

const request = new Request(`/api/status/update`);
let timeBefore;

setInterval( () => requestApi(), 1000);

function requestApi() {
    fetch(request).then(response => {
        if (response.status === 200) {
            return response.json();
        } else {
            throw new Error('Something went wrong on api server! The response has the status code: ' + response.status);
        }
    }).then(data => {
        appendData(data);
    }).catch(error => {
        console.error(error);
    })
}

function appendData(data) {
    document.getElementById("username").innerText = data.status.id;

    let statusElement = document.getElementById("status");
    let status = data.status.activityStatus;
    if (status) {
        statusElement.setAttribute("class", "green")
    } else {
        statusElement.setAttribute("class", "red")
    }
    statusElement.innerText = status;

    if (timeBefore === undefined) {
        timeBefore = data.totalTime;
    }
    if (timeBefore < data.totalTime) {
        let timeElement = document.getElementById("time");
        animateValue(timeElement, 0, data.totalTime, 2000);
        timeBefore = data.totalTime;
    }
}