package berufsschule.ae

import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.routing.put
import io.ktor.routing.route
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit

data class Status(val id: String, val activityStatus: Boolean)

object Activities {
    @Volatile
    var totalTime: Long = 0
    @Volatile
    var status = Status("xxx", false)
    private var currentActiveTime: LocalDateTime = LocalDateTime.now()

    @Synchronized
    fun update(status: Status): Boolean {
        if (this.status.activityStatus == status.activityStatus) {
            return false
        } else {
            if (!this.status.activityStatus) {
                currentActiveTime = LocalDateTime.now()
            } else {
                totalTime += ChronoUnit.SECONDS.between(currentActiveTime, LocalDateTime.now())
            }
            this.status = status
        }
        return true
    }
}

fun Route.restApiRouter() {

    route("/api/status") {
        put("/update") {
            Activities.update(call.receive())
            call.respond(HttpStatusCode.OK)
        }

        get("/update") {
            call.respond(Activities)
        }
    }

}