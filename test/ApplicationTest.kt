import berufsschule.ae.module
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.http.cio.websocket.Frame
import io.ktor.http.cio.websocket.readText
import io.ktor.server.testing.handleRequest
import io.ktor.server.testing.withTestApplication
import kotlinx.coroutines.channels.ClosedSendChannelException
import org.junit.Test
import kotlin.test.assertEquals

class ApplicationTest {

    @Test
    fun testRoot() {
        withTestApplication({ module(testing = true) }) {
            handleRequest(HttpMethod.Get, "/").apply {
                assertEquals(HttpStatusCode.OK, response.status())
                assertEquals("HELLO WORLD!", response.content)
            }
        }
    }

    @Test(expected = ClosedSendChannelException::class)
    fun testWebSockets() {
        withTestApplication({ module(testing = true) }) {
            handleWebSocketConversation("/myws/echo") { incoming, outgoing ->
                assertEquals("Hi from server", (incoming.receive() as Frame.Text).readText())
                outgoing.send(Frame.Text("foo"))
                assertEquals("Client said: foo", (incoming.receive() as Frame.Text).readText())
                outgoing.close()
                    outgoing.send(Frame.Text("foo"))
            }
        }
    }
}
