import io.kotlintest.matchers.startWith
import io.kotlintest.should
import io.kotlintest.shouldBe
import kotlin.test.Test

class KotlinTest {

    @Test
    fun shouldBeTest() {
        "hello".length shouldBe 5
    }

    @Test
    fun startWithTest() {
        "world" should startWith("wor")
    }
}