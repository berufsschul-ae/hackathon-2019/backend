import berufsschule.ae.Activities
import berufsschule.ae.Status
import berufsschule.ae.module
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import io.kotlintest.matchers.string.shouldContain
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.server.testing.handleRequest
import io.ktor.server.testing.setBody
import io.ktor.server.testing.withTestApplication
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import kotlin.test.Test

internal class RestApiRouterKtTest {

    @Test
    fun testPutStatusUpdateWithWrongBody() {
        withTestApplication({
            module()
        }) {
            val activities = Activities
            assertEquals(activities, Activities)
            handleRequest(HttpMethod.Put, "/api/status/update") {
                setBody("foo")
            }.apply {
                assertEquals(HttpStatusCode.UnsupportedMediaType, response.status())
            }
            assertEquals(activities, Activities)
        }
    }

    @Test
    fun testPutStatusUpdate() {
        withTestApplication({
            module()
        }) {
            handleRequest(HttpMethod.Put, "/api/status/update") {
                val s = jacksonObjectMapper().writeValueAsString(Status("xxx", true))
                setBody(s)
                addHeader("Content-Type", "application/json")
            }.apply {
                assertEquals(HttpStatusCode.OK, response.status())
            }
        }
    }

    @Test
    fun testGetStatusUpdate() {
        withTestApplication({
            module()
        }) {
            handleRequest(HttpMethod.Get, "/api/status/update") { setBody("foo") }.apply {
                assertEquals(HttpStatusCode.OK, response.status())
            }

        }
    }

    @Test
    fun testStatusUpdateApi() {
        withTestApplication({
            module()
        }) {
            handleRequest(HttpMethod.Put, "/api/status/update") {
                val s = jacksonObjectMapper().writeValueAsString(Status("xxx", true))
                setBody(s)
                addHeader("Content-Type", "application/json")
            }.apply {
                assertEquals(HttpStatusCode.OK, response.status())
            }
            runBlocking { delay(10000) }
            handleRequest(HttpMethod.Put, "/api/status/update") {
                val s = jacksonObjectMapper().writeValueAsString(Status("xxx", false))
                setBody(s)
                addHeader("Content-Type", "application/json")
            }.apply {
                assertEquals(HttpStatusCode.OK, response.status())
            }
            handleRequest(HttpMethod.Get, "/api/status/update").apply {
                assertEquals(HttpStatusCode.OK, response.status())
                val s = response.content
                s.shouldContain("\"totalTime\" : 10")
            }
        }
    }
}